FROM registry.gitlab.inria.fr/jpesek/mechanical-networks/ubuntu/base:22.04

#Get build date and version
ARG BUILD_DATE
ARG VERSION

#Mark the file
LABEL build-date=$BUILD_DATE
LABEL name="devel-ubuntu-22.04"
LABEL description="Ubuntu 22.04 Focal Jammy Jellyfish for building MechanicalNetworks with all the developper tools."
LABEL usage="https://gitlab.inria.fr/jpesek/mechanical-networks/-/wikis/home"
LABEL vsc-url="https://gitlab.inria.fr/jpesek/mechanical-networks"
LABEL version=$VERSION

#Prepare tzdata - set timezone to Brussels
RUN ln -fs /usr/share/zoneinfo/Europe/Brussels /etc/localtime

#Set the frontend non-interactive
ENV DEBIAN_FRONTEND noninteractive

#Upgrade and install dependencies
# - ccmake
# - cgdb (debugger)
# - clang-format (formatter and format checker)
# - clang-tidy (static code analyzer)
# - gawk (String processor)
# - gcovr (Code coverage statistic)
# - gdb (debugger)
# - kcachegrind (memory detection and profiler)
# - libgmock-dev (GoogleTest mock library)
# - libgtest-dev (GoogleTest library)
# - perf (profiler)
# - valgrind (profiler and memory sanitizer)
# - vim
RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y \
                    cgdb \
                    clang-format \
                    clang-tidy \
                    cmake-curses-gui \
                    gawk \
                    gcovr \
                    gdb \
                    kcachegrind \
                    libgmock-dev \
                    libgtest-dev \
                    linux-tools-common \
                    valgrind \
                    vim \
    && apt-get clean

CMD "/bin/bash"
