FROM ubuntu:22.04 

#Get build date and version
ARG BUILD_DATE
ARG VERSION

#Mark the file
LABEL build-date=$BUILD_DATE
LABEL name="base-ubuntu-22.04"
LABEL description="Ubuntu 22.04 Focal Jammy Jellyfish for building MechanicalNetworks."
LABEL usage="https://gitlab.inria.fr/jpesek/mechanical-networks/-/wikis/home"
LABEL vsc-url="https://gitlab.inria.fr/jpesek/mechanical-networks"
LABEL version=$VERSION

#Prepare tzdata - set timezone to Brussels
RUN ln -fs /usr/share/zoneinfo/Europe/Brussels /etc/localtime

#Set the frontend non-interactive
ENV DEBIAN_FRONTEND noninteractive

#Upgrade and install dependencies
#Base:
# - build-essential
# - clang
# - cmake
# - gcc
# - git
# - tzdata
#Doxygen:
# - dia
# - mscgen
# - doxygen
# - doxygen-latex
# - graphviz
# - texlive
RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y \
                    build-essential \
                    clang \
                    cmake \
                    dia \
                    doxygen \
                    doxygen-latex \
                    gcc \
                    git \
                    graphviz \
                    mscgen \
                    texlive \
                    tzdata \
    && apt-get clean

CMD "/bin/bash"
