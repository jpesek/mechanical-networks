FROM registry.gitlab.inria.fr/jpesek/mechanical-networks/ubuntu/devel:22.04

#These variables should be passed from environment
ARG DOCKER_USER
ARG DOCKER_TOKEN
ARG PROJECT_URL
ARG BUILD_DATE
ARG VSC_REF
ARG VERSION

LABEL build-date=$BUILD_DATE
LABEL name="mechanical-networks-devel-ubuntu-22.04"
LABEL description="Ubuntu 22.04 Jammy Jellyfish with installed Mechanical networks, using GCC"
LABEL usage="https://gitlab.inria.fr/jpesek/mechanical-networks/-/wikis/home"
LABEL vsc-url="https://gitlab.inria.fr/jpesek/mechanical-networks"
LABEL vsc-ref=$VSC_REF
LABEL version=$VERSION

#Install nginx server for hosting the documentation
RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y nginx-light
# ... and set the outgoing port
EXPOSE 80

#Build and install our codebase
RUN    git clone --single-branch https://$DOCKER_USER:$DOCKER_TOKEN@$PROJECT_URL ./src \
    && cmake -D CMAKE_BUILD_TYPE=Release \
             -D CMAKE_INSTALL_DOCDIR=/var/www/html \
             -D INCLUDE_DOCUMENTATION=ON \
             -D INCLUDE_TESTS=ON \
             -S src/CMake \
             -B build \
    && cmake --build build -j \
    && cmake --build build --target documentation \
    && cmake --build build --target install \
    && rm -r build \
    && rm -r src

#Re-discover all libraries
RUN ldconfig

#Prepare the start script
# - launch nginx
# - launch bash
RUN echo "#!/bin/bash\nnginx\nbash" > /usr/local/bin/run-docker.sh \
    && chmod +x /usr/local/bin/run-docker.sh

CMD "/usr/local/bin/run-docker.sh"
