#Do we include tests
option( INCLUDE_TESTS "Add necessary targets for all tests inside the build." OFF )
if( INCLUDE_TESTS )
    #Set valgrind options 
    set( MEMORYCHECK_COMMAND_OPTIONS "--tool=memcheck --leak-check=full --error-exitcode=1" )

    #Include Ctest as the basic framework and enable testing
    include(CTest)
    enable_testing()

    #Enable Google Test
    find_package( GTest REQUIRED )

    #Find threads library - needed for GTest
    find_package( Threads REQUIRED )

    #--------------------------
    # Basic interface
    #--------------------------
    #Define the basic target
    add_library( Tests INTERFACE )

    #Add required dependencies - this list is supposed to be minimal
    # - added link to library
    # - added link to Threads
    target_link_libraries( Tests
                           INTERFACE ${CMAKE_PROJECT_NAME}
                           INTERFACE Threads::Threads
                           )

    #Add correct version of GTest
    if( "${CMAKE_VERSION}" VERSION_LESS "3.20" )
        target_link_libraries( Tests
                               INTERFACE GTest::Main
                               INTERFACE GTest::GTest
                               INTERFACE gmock
                               )
    elseif( "${CMAKE_VERSION}" VERSION_LESS "3.23" )
        target_link_libraries( Tests
                               INTERFACE GTest::gtest_main
                               INTERFACE GTest::gtest
                               INTERFACE gmock
                               )
    else()
        target_link_libraries( Tests
                               INTERFACE GTest::gmock_main
                               INTERFACE GTest::gtest
                               INTERFACE GTest::gmock
                               )
    endif()

    #--------------------------
    # Actual tests
    #--------------------------
    #Find all tests
    file( GLOB_RECURSE TEST_FILES RELATIVE "${PROJECT_SOURCE_DIR}/${CMAKE_PROJECT_NAME}" "${PROJECT_SOURCE_DIR}/**/*.cpp" )
    list( FILTER TEST_FILES INCLUDE REGEX "Tests/" )

    message( STATUS "Found tests:" )
    #Get all normal tests (non failing ones)
    foreach( TEST_FILE ${TEST_FILES} )
        #Get filename, folder and name with path information
        get_filename_component( TEST_FILENAME ${TEST_FILE} NAME_WE )
        get_filename_component( TEST_FOLDER ${TEST_FILE} DIRECTORY )
        set( TEST_TARGET "${TEST_FILE}" )        
        string( REPLACE "/" "_" TEST_TARGET ${TEST_TARGET} )
        string( REPLACE ".cpp" "" TEST_TARGET ${TEST_TARGET} )

        #Prepare the test name
        message( STATUS " - ${TEST_FILE}" )

        #Add target for the file
        add_executable( ${TEST_TARGET} "${PROJECT_SOURCE_DIR}/${CMAKE_PROJECT_NAME}/${TEST_FILE}" )

        #Set the build path 
        string( REPLACE "/Tests/" "/" TEST_TARGET_FOLDER "${TEST_FOLDER}" )
        string( REGEX REPLACE "/Tests$" "" TEST_TARGET_FOLDER "${TEST_TARGET_FOLDER}" )
        set_property( TARGET ${TEST_TARGET} PROPERTY RUNTIME_OUTPUT_DIRECTORY Testing/${CMAKE_PROJECT_NAME}/${TEST_TARGET_FOLDER} )

        #Override filename
        set_property( TARGET ${TEST_TARGET} PROPERTY OUTPUT_NAME ${TEST_FILENAME} )

        target_link_libraries( ${TEST_TARGET}
                               PRIVATE Tests
                             )

        #Register test
        gtest_discover_tests( ${TEST_TARGET} 
                              EXTRA_ARGS "--gtest_death_test_style=threadsafe"
                              PROPERTIES TIMEOUT 60
                            )
    endforeach()
endif()
