option( INCLUDE_DOCUMENTATION "Add a specific target 'documentation' for generating the documentation." OFF )
if( INCLUDE_DOCUMENTATION )
    find_package(Doxygen 1.8.14 REQUIRED dot dia OPTIONAL_COMPONENTS mscgen )
    
    #Allow user to pick up what they want to generate
    set( DOXYGEN_GENERATE_HTML YES CACHE STRING "Should we generate HTML documentation." )
    set( DOXYGEN_GENERATE_MAN NO CACHE STRING "Should we generate man pages." )
    set( DOXYGEN_TAGFILE "" CACHE STRING "Filename containing the tags. Will be stored in Documentation." )

    set_property( CACHE DOXYGEN_GENERATE_HTML PROPERTY STRINGS YES NO )
    set_property( CACHE DOXYGEN_GENERATE_MAN PROPERTY STRINGS YES NO )

    mark_as_advanced( DOXYGEN_GENERATE_HTML DOXYGEN_GENERATE_MAN DOXYGEN_TAGFILE )

    # Allow pdflatex
    set( DOXYGEN_USE_PDFLATEX YES ) 

    # Add ams latex packages to EXTRA_PACKAGES doxyfile options
    set( DOXYGEN_EXTRA_PACKAGES amsmath amsfonts amssymb )

    # Allow clang to deal with templates and parsing? 
    set( DOXYGEN_CLANG_ASSISTED_PARSING NO CACHE STRING "Should we use clang to parse the files." )
    set_property( CACHE DOXYGEN_CLANG_ASSISTED_PARSING PROPERTY STRINGS YES NO )
    mark_as_advanced( DOXYGEN_CLANG_ASSISTED_PARSING )

    #If so set up all necessary variables
    if( DOXYGEN_CLANG_ASSISTED_PARSING STREQUAL "YES" )
        # We need to export compile commands so we can infer compile options
        set( CMAKE_EXPORT_COMPILE_COMMANDS ON )
        set( DOXYGEN_CLANG_DATABASE_PATH "${CMAKE_CURRENT_BINARY_DIR}" )
    endif()

    # Set from where we consider the paths relative 
    set( DOXYGEN_FULL_PATH_NAMES YES )
    set( DOXYGEN_STRIP_FROM_PATH "${PROJECT_SOURCE_DIR}" )   
    set( DOXYGEN_STRIP_FROM_INC_PATH "${PROJECT_SOURCE_DIR}" )
    
    # Set path to images used for documentation
    #set( DOXYGEN_IMAGE_PATH "${PROJECT_SOURCE_DIR}/Documentation/Images/" )
    
    #Output will be in the build directory in folder documentation by default
    set( DOXYGEN_OUTPUT_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/Documentation" )

    #Which files should be ommited
    #So far non-compiling tests and README files.
    set( DOXYGEN_EXCLUDE_PATTERNS README.md )
    set( DOXYGEN_EXCLUDE_SYMBOLS "MechanicalNetworks::*::Tests" )

    #Extract private virtual methods
    set( DOXYGEN_EXTRACT_PRIV_VIRTUAL YES )

    #Doxygen should fail on Warning
    set( DOXYGEN_WARN_AS_ERROR NO CACHE STRING "Should we fail the build if the documentation is incomplete?" )
    set_property( CACHE DOXYGEN_WARN_AS_ERROR PROPERTY STRINGS YES NO FAIL_ON_WARNINGS )
    mark_as_advanced( DOXYGEN_WARN_AS_ERROR )

    #Set directories to be parsed by doxygen
    list( APPEND DIRECTORIES_TO_DOCUMENT "${PROJECT_SOURCE_DIR}/Documentation/" )

    #Generate target for documentation
    doxygen_add_docs( documentation ${DIRECTORIES_TO_DOCUMENT} COMMENT "Generate documentation." )

    #Enable installation
    if( ${DOXYGEN_GENERATE_HTML} STREQUAL "YES" )
        install( DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/Documentation/html/" DESTINATION "${CMAKE_INSTALL_DOCDIR}" )
    endif()
    if( ${DOXYGEN_GENERATE_MAN} STREQUAL "YES" )
        install( DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/Documentation/man/" TYPE MAN )
    endif()
    if( DOXYGEN_TAGFILE )
        set( DOXYGEN_GENERATE_TAGFILE "${CMAKE_CURRENT_BINARY_DIR}/Documentation/${DOXYGEN_TAGFILE}" )
        install( FILES "${DOXYGEN_GENERATE_TAGFILE}" DESTINATION "${CMAKE_INSTALL_DOCDIR}" )
    endif()
endif()

