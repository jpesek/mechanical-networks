#Get all source and header files
file( GLOB_RECURSE SOURCE_FILES LIST_DIRECTORIES false "${PROJECT_SOURCE_DIR}/**/*.h" "${PROJECT_SOURCE_DIR}/**/*.cpp" )
#Filter out build folder in case it is included
list( FILTER SOURCE_FILES EXCLUDE REGEX "^${CMAKE_CURRENT_BINARY_DIR}" )

#Add target to run clang-format on all code base and print report on standard output
add_custom_target( check-format
                   COMMAND clang-format
                           --dry-run
                           --Werror
                           -style=file
                           ${SOURCE_FILES} 
                 )


