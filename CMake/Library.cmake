#Find all files forming the library
file( GLOB_RECURSE LIB_SOURCE_FILES LIST_DIRECTORIES false RELATIVE "${PROJECT_SOURCE_DIR}/${CMAKE_PROJECT_NAME}" "${PROJECT_SOURCE_DIR}/${CMAKE_PROJECT_NAME}/*.cpp" "${PROJECT_SOURCE_DIR}/${CMAKE_PROJECT_NAME}/**/*.cpp" )
#Filter out tests
list( FILTER LIB_SOURCE_FILES EXCLUDE REGEX "/Tests" )
list( FILTER LIB_SOURCE_FILES EXCLUDE REGEX "^Tests" )

#Set-up target 
if( LIB_SOURCE_FILES )
    #Define the target
    message( STATUS "Found library ${CMAKE_PROJECT_NAME}:" )
    add_library( ${CMAKE_PROJECT_NAME} SHARED )
    target_link_libraries( ${CMAKE_PROJECT_NAME} PUBLIC Coverage )

    #Add install sequence
    install( TARGETS ${CMAKE_PROJECT_NAME} 
             EXPORT ${CMAKE_PROJECT_NAME}Targets
             LIBRARY 
             INCLUDES DESTINATION include
             )
    #... and prepare for export
    install( EXPORT ${CMAKE_PROJECT_NAME}Targets 
             FILE "${CMAKE_PROJECT_NAME}Targets.cmake"
             NAMESPACE "${CMAKE_PROJECT_NAME}::"
             DESTINATION "lib/cmake/${CMAKE_PROJECT_NAME}"
             )
endif()
#... add all sources
foreach( SOURCE_FILE ${LIB_SOURCE_FILES} )
    message( STATUS " - ${SOURCE_FILE}" )
    target_sources( ${CMAKE_PROJECT_NAME} PRIVATE "${PROJECT_SOURCE_DIR}/${CMAKE_PROJECT_NAME}/${SOURCE_FILE}" )
endforeach()

#... find all relevant header files
file( GLOB_RECURSE LIB_HEADER_FILES LIST_DIRECTORIES false RELATIVE "${PROJECT_SOURCE_DIR}" "${PROJECT_SOURCE_DIR}/${CMAKE_PROJECT_NAME}/*.h" "${PROJECT_SOURCE_DIR}/${CMAKE_PROJECT_NAME}/**/*.h" )
#Filter out tests
list( FILTER LIB_HEADER_FILES EXCLUDE REGEX "/Tests" )
list( FILTER LIB_HEADER_FILES EXCLUDE REGEX "^Tests" )
#... add them to export
foreach( HEADER_FILE ${LIB_HEADER_FILES} )
    get_filename_component( HEADER_DIR ${HEADER_FILE} DIRECTORY )
    install( FILES "${PROJECT_SOURCE_DIR}/${HEADER_FILE}" 
             DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}/${HEADER_DIR}" 
             )
endforeach()

#Add library to documentation 
list( APPEND DIRECTORIES_TO_DOCUMENT "${PROJECT_SOURCE_DIR}/${CMAKE_PROJECT_NAME}" )
