#Find all files forming the executable
file( GLOB_RECURSE EXE_SOURCE_FILES LIST_DIRECTORIES false RELATIVE "${PROJECT_SOURCE_DIR}/CLI" "${PROJECT_SOURCE_DIR}/CLI/*.cpp" "${PROJECT_SOURCE_DIR}/CLI/**/*.cpp" )
#Filter out tests
list( FILTER EXE_SOURCE_FILES EXCLUDE REGEX "/Tests" )
list( FILTER EXE_SOURCE_FILES EXCLUDE REGEX "^Tests" )

#Set-up target 
if( EXE_SOURCE_FILES )
    #Define the target
    message( STATUS "Found binary CLI:" )
    add_executable( stiffness )
    target_link_libraries( stiffness PRIVATE ${CMAKE_PROJECT_NAME} )

    #Add install sequence
    install( TARGETS stiffness 
             RUNTIME
             )
endif()
#... add all sources
foreach( SOURCE_FILE ${EXE_SOURCE_FILES} )
    message( STATUS " - ${SOURCE_FILE}" )
    target_sources( stiffness PRIVATE "${PROJECT_SOURCE_DIR}/CLI/${SOURCE_FILE}" )
endforeach()

#Add library to documentation 
list( APPEND DIRECTORIES_TO_DOCUMENT "${PROJECT_SOURCE_DIR}/CLI" )
