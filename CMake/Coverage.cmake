#Define the coverage interface
add_library( Coverage INTERFACE )

#Do we generate coverage options?
option( GENERATE_COVERAGE_REPORT "Generate coverage report in Corbertura XML format." OFF )
mark_as_advanced( GENERATE_COVERAGE_REPORT )

#Add coverage option - only for gcc
if( GENERATE_COVERAGE_REPORT AND NOT (CMAKE_CXX_COMPILER_ID STREQUAL "GNU") )
    message( SEND_ERROR "Coverage report is supported only on gcc." )
endif()

if( INCLUDE_TESTS AND GENERATE_COVERAGE_REPORT ) 
    #Set coverage options 
    target_compile_options( Coverage INTERFACE --coverage )
    target_link_libraries( Coverage INTERFACE gcov )
endif()

#Add install sequence
install( TARGETS Coverage 
         EXPORT CoverageTargets
         )
#Export also cmake when exporting headers - most likely we will compile
install( EXPORT CoverageTargets 
         NAMESPACE "${CMAKE_PROJECT_NAME}::"
         DESTINATION "lib/cmake/${CMAKE_PROJECT_NAME}"
         )

#Do we have coverage
if( GENERATE_COVERAGE_REPORT )
    #Generate the dummy targets
    add_custom_target(coverage-folder COMMAND ${CMAKE_COMMAND} -E make_directory Testing/Coverage )
    add_custom_target(coverage COMMAND gcovr --xml-pretty
                                       --exclude-unreachable-branches 
                                       --exclude [[".*/Tests/.*"]]
                                       --print-summary
                                       -o coverage.xml 
                                       --root "${PROJECT_SOURCE_DIR}"
                                       --object-directory ${CMAKE_BINARY_DIR}
                               COMMAND gcovr --html-details
                                       --exclude-unreachable-branches 
                                       --exclude [[".*/Tests/.*"]]
                                       --delete
                                       -o coverage.html
                                       --root "${PROJECT_SOURCE_DIR}"
                                       --object-directory "${CMAKE_BINARY_DIR}"
                               DEPENDS ${CMAKE_PROJECT_NAME} coverage-folder
                               BYPRODUCTS **/*.gcno **/*.gcda
                               WORKING_DIRECTORY Testing/Coverage
                               COMMENT "Generating test coverage report"
                      )
endif()
