#ifndef MN_STIFFNESS_H
#define MN_STIFFNESS_H

#include "Exceptions/Exceptions.h"

namespace MechanicalNetworks {
    /*! \brief Parse the string and return an equivalent stiffness
     *
     * Practically calls IO::parse() with Elements::ElementBase::stiffness() on the result.
     *
     * Assumptions:
     *  * String represents a single circuit.
     *
     * Parsing rules:
     *  * Any white space characters (`' '`,`'\\r'`,`'\\n'`,`'\\t'`) are ommited.
     *  * Tokens '[' and '(' correspond elements in parallel and series respectivelly.
     *  * Token separator are ',',']',')' and the opening brackets.
     *  * Closing brackets must match opening brackets.
     *  * Any other token is interpreted as a number.
     *  * Empty string is interpreted as spring with stiffness infty.
     *
     * \param str is input string which we will parse
     * \returns Elements representing the input string.
     * \throws Exceptions::ParsingError whenever ill formatted input is met.
     * \throws Exceptions::InvalidArgument whenever spring with negative stiffness is met.
     *
     * \sa IO::parse, Elements
     */
    auto stiffness(const std::string& str) -> double;
}
#endif
