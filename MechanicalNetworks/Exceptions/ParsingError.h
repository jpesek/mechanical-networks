#ifndef NM_PARSINGERROR_H
#define NM_PARSINGERROR_H

#include "MechanicalNetworkException.h"

namespace MechanicalNetworks::Exceptions {
    /*! \brief Exception for issues with parsing
     *
     * Used when parsing ill-formatted input.
     */
    class ParsingError: public MechanicalNetworkException
    {
      public:
        //! Inherit constructors
        using MechanicalNetworkException::MechanicalNetworkException;
    };
}
#endif
