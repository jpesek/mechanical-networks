#include <gtest/gtest.h>

#include "MechanicalNetworks/Exceptions/InitializationError.h"

//! Namespace containing tests of exceptions
namespace MechanicalNetworks::Exceptions::Tests {
    void except()
    {
        throw InitializationError("General exception.");
    }

    TEST(TestInitializationError, BasicTests)
    {
        //Check we get correct exception
        EXPECT_THROW(except(), InitializationError);

        //And that it can be casted to ancestor
        EXPECT_THROW(except(), MechanicalNetworkException);
        EXPECT_THROW(except(), std::runtime_error);
    }
}
