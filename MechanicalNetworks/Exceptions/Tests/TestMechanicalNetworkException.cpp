#include <gtest/gtest.h>

#include "MechanicalNetworks/Exceptions/MechanicalNetworkException.h"

//! Namespace containing tests of exceptions
namespace MechanicalNetworks::Exceptions::Tests {
    void except()
    {
        throw MechanicalNetworkException("General exception.");
    }

    TEST(TestMechanicalNetworkException, BasicTests)
    {
        //Check we get correct excpetion
        EXPECT_THROW(except(), MechanicalNetworkException);

        //And that it can be casted to ancestor
        EXPECT_THROW(except(), std::runtime_error);
    }
}
