#include <gtest/gtest.h>

#include "MechanicalNetworks/Exceptions/InvalidArgument.h"

//! Namespace containing tests of exceptions
namespace MechanicalNetworks::Exceptions::Tests {
    void except()
    {
        throw InvalidArgument("General exception.");
    }

    TEST(TestInvalidArgument, BasicTests)
    {
        //Check we get correct excpetion
        EXPECT_THROW(except(), InvalidArgument);

        //And that it can be casted to ancestor
        EXPECT_THROW(except(), MechanicalNetworkException);
        EXPECT_THROW(except(), std::runtime_error);
    }
}
