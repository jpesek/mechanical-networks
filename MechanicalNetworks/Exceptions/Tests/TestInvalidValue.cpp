#include <gtest/gtest.h>

#include "MechanicalNetworks/Exceptions/InvalidValue.h"

//! Namespace containing tests of exceptions
namespace MechanicalNetworks::Exceptions::Tests {
    void except()
    {
        throw InvalidValue("General exception.");
    }

    TEST(TestInvalidValue, BasicTests)
    {
        //Check we get correct excpetion
        EXPECT_THROW(except(), InvalidValue);

        //And that it can be casted to ancestor
        EXPECT_THROW(except(), MechanicalNetworkException);
        EXPECT_THROW(except(), std::runtime_error);
    }
}
