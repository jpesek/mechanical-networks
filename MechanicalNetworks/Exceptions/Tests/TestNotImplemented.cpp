#include <gtest/gtest.h>

#include "MechanicalNetworks/Exceptions/NotImplemented.h"

namespace MechanicalNetworks::Exceptions::Tests {
    void except()
    {
        throw NotImplemented("General exception.");
    }

    TEST(TestNotImplemented, BasicTests)
    {
        //Check we get correct excpetion
        EXPECT_THROW(except(), NotImplemented);

        //And that it can be casted to ancestor
        EXPECT_THROW(except(), MechanicalNetworkException);
        EXPECT_THROW(except(), std::runtime_error);
    }
}
