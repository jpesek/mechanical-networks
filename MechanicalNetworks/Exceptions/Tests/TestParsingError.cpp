#include <gtest/gtest.h>

#include "MechanicalNetworks/Exceptions/ParsingError.h"

//! Namespace containing tests of exceptions
namespace MechanicalNetworks::Exceptions::Tests {
    void except()
    {
        throw ParsingError("General exception.");
    }

    TEST(TestParsingError, BasicTests)
    {
        //Check we get correct excpetion
        EXPECT_THROW(except(), ParsingError);

        //And that it can be casted to ancestor
        EXPECT_THROW(except(), MechanicalNetworkException);
        EXPECT_THROW(except(), std::runtime_error);
    }
}
