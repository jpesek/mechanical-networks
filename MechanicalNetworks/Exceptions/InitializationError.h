#ifndef NM_INITIALIZATIONERROR_H
#define NM_INITIALIZATIONERROR_H

#include "MechanicalNetworkException.h"

namespace MechanicalNetworks::Exceptions {
    /*! \brief Exception for failed initialization
     *
     * Used when we failed allocate resources.
     * E.g. std::make_shared returns nullptr.
     */
    class InitializationError: public MechanicalNetworkException
    {
      public:
        //! Inherit constructors
        using MechanicalNetworkException::MechanicalNetworkException;
    };
}
#endif
