#ifndef NM_NOTIMPLEMENTED_H
#define NM_NOTIMPLEMENTED_H

#include "MechanicalNetworkException.h"

namespace MechanicalNetworks::Exceptions {
    /*! \brief Exception for case which is not implemented
     *
     * Exception thrown whentever given functionality is not provided or implemented.
     */
    class NotImplemented: public MechanicalNetworkException
    {
      public:
        using MechanicalNetworkException::MechanicalNetworkException;
    };
}
#endif
