#ifndef NM_INVALIDVALUE_H
#define NM_INVALIDVALUE_H

#include "MechanicalNetworkException.h"

namespace MechanicalNetworks::Exceptions {
    /*! \brief Exception for invalid value
     *
     * Used when we encounter invalid value in the processing.
     * E.g. nullptr where pointer was expected or negative value where positive value was expected.
     */
    class InvalidValue: public MechanicalNetworkException
    {
      public:
        //! Inherit constructors
        using MechanicalNetworkException::MechanicalNetworkException;
    };
}
#endif
