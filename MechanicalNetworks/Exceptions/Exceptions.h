#ifndef NM_EXCEPTIONS_H
#define NM_EXCEPTIONS_H

/*! \brief Namespace containing all exceptions
 *
 * Supported exceptions:
 * * MechanicalNetworkException - general ancestor of all exceptions we provide
 *   * InvalidArgument - argument passed to function is not from a valid range
 *   * InitializationError - acquiring of resources failed.
 *   * InvalidValue - unexpected value when processing data
 *   * NotImplemented - given functionality is not implemented yet
 *   * ParsingError - when parsing ill-formatted input
 */
namespace MechanicalNetworks::Exceptions {
}

#include "InitializationError.h"
#include "InvalidArgument.h"
#include "InvalidValue.h"
#include "MechanicalNetworkException.h"
#include "NotImplemented.h"
#include "ParsingError.h"
#endif
