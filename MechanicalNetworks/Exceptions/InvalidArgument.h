#ifndef NM_INVALIDARGUMENT_H
#define NM_INVALIDARGUMENT_H

#include "MechanicalNetworkException.h"

namespace MechanicalNetworks::Exceptions {
    /*! \brief Exception for invalid argument
     *
     * Used when the given value of the argument is not acceptable.
     * E.g. non-positive stiffness.
     */
    class InvalidArgument: public MechanicalNetworkException
    {
      public:
        //! Inherit constructors
        using MechanicalNetworkException::MechanicalNetworkException;
    };
}
#endif
