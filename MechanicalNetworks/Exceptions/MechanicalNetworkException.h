#ifndef NM_MECHANICALNETWORKEXCEPTION_H
#define NM_MECHANICALNETWORKEXCEPTION_H

#include <stdexcept>

namespace MechanicalNetworks::Exceptions {
    /*! \brief Common ancestor to all exceptions
     *
     * Common base of all exceptions we provide.
     */
    class MechanicalNetworkException: public std::runtime_error
    {
      public:
        //! Inherit constructors
        using std::runtime_error::runtime_error;
    };
}
#endif
