#ifndef MN_ELEMENTSTORAGE_IMPL_H
#define MN_ELEMENTSTORAGE_IMPL_H

#include "ElementStorage.h"

namespace MechanicalNetworks::Elements {
    template<class T, class... Args>
    auto ElementStorage::emplace_back(Args&&... args) -> std::shared_ptr<T>
    {
        auto el = T::create(std::forward<Args>(args)...);
        if(! el) {
            throw Exceptions::InitializationError("In ElementStorage::emplace_back() memory allocation failed.");
        }

        storage_.push_back(el);
        return el;
    }
}
#endif
