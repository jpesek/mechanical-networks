#include "LinearSpring.h"

namespace MechanicalNetworks::Elements {
    LinearSpring::LinearSpring(Token token, double stiffness)
        : base(std::move(token))
        , k_{ std::move(stiffness) }
    {
        if(k_ < 0) {
            throw Exceptions::InvalidArgument("Stiffness has to be non-negative number.");
        }
    }

    auto LinearSpring::clone() const -> std::shared_ptr<ElementBase>
    {
        return std::make_shared<LinearSpring>(*this);
    }

    auto LinearSpring::stiffness() const -> double
    {
        return k_;
    }
}
