#ifndef MN_ELEMENTFACTORY_IMPL_H
#define MN_ELEMENTFACTORY_IMPL_H

#include "ElementFactory.h"

namespace MechanicalNetworks::Elements {
    template<class T, class Base>
    template<class... Args>
    auto ElementFactory<T, Base>::create(Args&&... args) -> std::shared_ptr<T>
    {
        return std::make_shared<T>(typename Base::Token{}, std::forward<Args>(args)...);
    }
}
#endif
