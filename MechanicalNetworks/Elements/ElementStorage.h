#ifndef MN_ELEMENTSTORAGE_H
#define MN_ELEMENTSTORAGE_H

#include <iterator>
#include <vector>

#include "MechanicalNetworks/Exceptions/Exceptions.h"
#include "detail/PointerIterator.h"

#include "ElementBase.h"

namespace MechanicalNetworks::Elements {
    /*! \brief Storage of elements
     *
     * Container allowing us to store multiple elements and add them in a row.
     * It also provides iterators to access the content of the storage.
     */
    class ElementStorage: public ElementBase
    {
      public:
        //! Inherit constructors
        using ElementBase::ElementBase;

        //! Define used container type
        using container_type = std::vector<std::shared_ptr<ElementBase>>;

        //! Define iterator type
        using iterator = detail::PointerIterator<typename container_type::iterator>;

        //! Define const iterator type
        using const_iterator = detail::PointerIterator<typename container_type::const_iterator>;

        //! Define reverse iterator type
        using reverse_iterator = std::reverse_iterator<iterator>;

        //! Define const reverse iterator type
        using const_reverse_iterator = std::reverse_iterator<const_iterator>;

        /*! \brief Appends an element to the storage
         *
         * Copies the pointer el at the end of the storage.
         *
         * \param el is element which will be added
         * \throws Exceptions::InvalidArgument if el is nullptr
         */
        void push_back(const std::shared_ptr<ElementBase>& el);

        /*! \brief Appends an element to the storage
         *
         * Moves the pointer at the end of the storage.
         *
         * \param el is element which will be added
         * \throws Exceptions::InvalidArgument if el is nullptr
         */
        void push_back(std::shared_ptr<ElementBase>&& el);

        /*! \brief Helper function for inserting the elements in place
         *
         * Creates a new instance of T and place it at the end.
         * Equivalent to:
         * \code{.cpp}
         * auto x = T::create( args... );
         * storage->push_back(x);
         * \endcode
         *
         * \tparam Args are types of the additional argument
         * \param args are additional parameters to constructor of T
         * \returns shared_ptr to the newly added element.
         * \throws Exceptions::InitializationError in case memory allocation fails.
         */
        template<class T, class... Args>
        auto emplace_back(Args&&... args) -> std::shared_ptr<T>;

        /*! \brief Empty the storage
         *
         * Clears the storage.
         * Afterwards, `size() == 0` and `bool(*this) == false`.
         */
        void clear();

        /*! \brief Returns number of stored elements
         *
         * \returns number or elements in this storage
         */
        auto size() const -> std::size_t;

        /*! \brief Returns true if the storage is not empty
         *
         * \returns `size()>0`
         */
        operator bool() const;

        //! Returns iterator pointing to first element
        auto begin() -> iterator;

        //! Returns const iterator pointing to first element
        auto begin() const -> const_iterator;

        //! Returns const iterator pointing to first element
        auto cbegin() const -> const_iterator;

        //! Returns iterator pointing beyond the end
        auto end() -> iterator;

        //! Returns const iterator pointing beyond the end
        auto end() const -> const_iterator;

        //! Returns const iterator pointing beyond the end
        auto cend() const -> const_iterator;

        //! Returns reverse iterator pointing to the last element
        auto rbegin() -> reverse_iterator;

        //! Returns const reverse iterator pointing to the last element
        auto rbegin() const -> const_reverse_iterator;

        //! Returns const reverse iterator pointing to the last element
        auto crbegin() const -> const_reverse_iterator;

        //! Returns reverse iterator pointing before begin
        auto rend() -> reverse_iterator;

        //! Returns const reverse iterator pointing before begin
        auto rend() const -> const_reverse_iterator;

        //! Returns const reverse iterator pointing before begin
        auto crend() const -> const_reverse_iterator;

      private:
        //! Variable holding the storage
        container_type storage_{};
    };
}
#include "ElementStorage.impl.h"
#endif
