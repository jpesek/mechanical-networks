#ifndef MN_ELEMENTSINSERIES_H
#define MN_ELEMENTSINSERIES_H

#include "ElementFactory.h"
#include "ElementStorage.h"

namespace MechanicalNetworks::Elements {
    /*! \brief Element representing a set of elements in series
     *
     * The stiffness is given by the following relation:
     * \f[
     *  \frac{1}{k} = \sum\limits_i \frac{1}{k_i}.
     * \f]
     * In case the element is empty,
     * the resulting stiffness is \f$\infty\f$ as it represent a fixed connection.
     */
    class ElementsInSeries final: public ElementFactory<ElementsInSeries, ElementStorage>
    {
      public:
        //! Inherit constructors
        using base::base;

        /*! \brief Creates a deep copy of current element
         *
         * Creates new ElementsInSeries which it fill by results of clone() on individual elements it stores.
         *
         * \return deep copy of this
         * \throws Exceptions::InitializationError if memory allocation fails
         */
        auto clone() const -> std::shared_ptr<ElementBase> override;

        /*! \brief Compute stiffness of this element
         *
         * The stiffness is given by
         * \f[
         *  \frac{1}{k} = \sum\limits_i \frac{1}{k_i}.
         * \f]
         * In case the element is empty,
         * the resulting stiffness is \f$\infty\f$ as it represent a fixed connection.
         *
         * \returns \f$k\f$.
         */
        auto stiffness() const -> double override;
    };
}
#endif
