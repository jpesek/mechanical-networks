#include <cmath>
#include <gtest/gtest.h>

#include "MechanicalNetworks/Elements/ElementsInSeries.h"
#include "MechanicalNetworks/Elements/LinearSpring.h"

namespace MechanicalNetworks::Elements::Tests {
    constexpr double tolerance = 1e-5;

    TEST(TestElementsInSeries, Empty)
    {
        //Empty should be equivalent to infty
        auto empty = ElementsInSeries::create();
        EXPECT_TRUE(std::isinf(empty->stiffness()));
    }

    TEST(TestElementsInSeries, WithValues)
    {
        //Create
        auto el = ElementsInSeries::create();

        //Populate it with a new value (const ref)
        auto spr = LinearSpring::create(5.);
        el->push_back(spr);
        EXPECT_NEAR(5, el->stiffness(), 5 * tolerance);
        EXPECT_NEAR(5, spr->stiffness(), 5 * tolerance);

        //Add a new value (rvalue)
        el->push_back(LinearSpring::create(7.));
        EXPECT_NEAR(35. / 12., el->stiffness(), 35. / 12. * tolerance);

        //Emplace a new value
        auto spr2 = el->emplace_back<LinearSpring>(9.);
        EXPECT_NEAR(315. / 143., el->stiffness(), 315. / 143. * tolerance);
        EXPECT_NEAR(9, spr2->stiffness(), 9 * tolerance);

        //Couple failure modes
        EXPECT_THROW(el->push_back(nullptr), Exceptions::InvalidArgument);

        std::shared_ptr<ElementBase> none{};
        EXPECT_THROW(el->push_back(none), Exceptions::InvalidArgument);
    }

    TEST(TestElementsInSeries, Clear)
    {
        //Create
        auto el = ElementsInSeries::create();

        //Fill in
        el->emplace_back<LinearSpring>(2.);
        el->emplace_back<LinearSpring>(1.);
        el->emplace_back<LinearSpring>(0.5);

        //Clear them all
        el->clear();
        EXPECT_TRUE(std::isinf(el->stiffness()));
    }

    TEST(TestElementsInSeries, Loop)
    {
        //Create
        auto el = ElementsInSeries::create();

        //Fill in
        el->emplace_back<LinearSpring>(2.);
        el->emplace_back<LinearSpring>(1.);
        el->emplace_back<LinearSpring>(0.5);

        const std::vector<double> expected_values = { 2, 1, 0.5 };

        //Ranged loop
        auto it_ref = expected_values.cbegin();
        for(const auto& val: *el) {
            ASSERT_NE(it_ref, expected_values.cend());
            EXPECT_NEAR(*it_ref, val.stiffness(), tolerance * (*it_ref));
            ++it_ref;
        }
        EXPECT_EQ(it_ref, expected_values.cend());
    }

    TEST(TestElementsInSeries, ConstLoop)
    {
        //Create
        auto el = ElementsInSeries::create();

        //Fill in
        el->emplace_back<LinearSpring>(2.);
        el->emplace_back<LinearSpring>(1.);
        el->emplace_back<LinearSpring>(0.5);

        const std::vector<double> expected_values = { 2, 1, 0.5 };

        //Loop
        auto it_ref = expected_values.cbegin();
        for(auto it = el->cbegin(); it != el->cend(); ++it) {
            ASSERT_NE(it_ref, expected_values.cend());
            EXPECT_NEAR(*it_ref, it->stiffness(), tolerance * (*it_ref));
            ++it_ref;
        }
        EXPECT_EQ(it_ref, expected_values.cend());

        //Alternative is via ranged loop over const pointer
        std::shared_ptr<const ElementsInSeries> cel = el;

        it_ref = expected_values.cbegin();
        for(const auto& val: *cel) {
            ASSERT_NE(it_ref, expected_values.cend());
            EXPECT_NEAR(*it_ref, val.stiffness(), tolerance * (*it_ref));
            ++it_ref;
        }
        EXPECT_EQ(it_ref, expected_values.cend());
    }

    TEST(TestElementsInSeries, ReverseLoop)
    {
        //Create
        auto el = ElementsInSeries::create();

        //Fill in
        el->emplace_back<LinearSpring>(2.);
        el->emplace_back<LinearSpring>(1.);
        el->emplace_back<LinearSpring>(0.5);

        const std::vector<double> expected_values = { 0.5, 1, 2 };

        //Loop
        auto it_ref = expected_values.cbegin();
        for(auto it = el->rbegin(); it != el->rend(); ++it) {
            ASSERT_NE(it_ref, expected_values.cend());
            EXPECT_NEAR(*it_ref, it->stiffness(), tolerance * (*it_ref));
            ++it_ref;
        }
        EXPECT_EQ(it_ref, expected_values.cend());
    }

    TEST(TestElementsInSeries, ConstReverseLoop)
    {
        //Create
        auto el = ElementsInSeries::create();

        //Fill in
        el->emplace_back<LinearSpring>(2.);
        el->emplace_back<LinearSpring>(1.);
        el->emplace_back<LinearSpring>(0.5);

        const std::vector<double> expected_values = { 0.5, 1, 2 };

        //Loop
        auto it_ref = expected_values.cbegin();
        for(auto it = el->crbegin(); it != el->crend(); ++it) {
            ASSERT_NE(it_ref, expected_values.cend());
            EXPECT_NEAR(*it_ref, it->stiffness(), tolerance * (*it_ref));
            ++it_ref;
        }
        EXPECT_EQ(it_ref, expected_values.cend());

        //Alternative is via loop over const pointer
        std::shared_ptr<const ElementsInSeries> cel = el;

        it_ref = expected_values.cbegin();
        for(auto it = el->rbegin(); it != el->rend(); ++it) {
            ASSERT_NE(it_ref, expected_values.cend());
            EXPECT_NEAR(*it_ref, it->stiffness(), tolerance * (*it_ref));
            ++it_ref;
        }
        EXPECT_EQ(it_ref, expected_values.cend());
    }

    TEST(TestElementsInSeries, LateBinding)
    {
        //Create
        auto el = ElementsInSeries::create();

        //Fill in
        el->emplace_back<LinearSpring>(2.);
        el->emplace_back<LinearSpring>(1.);
        el->emplace_back<LinearSpring>(0.5);

        std::shared_ptr<ElementBase> base = el;
        EXPECT_NEAR(1. / 3.5, base->stiffness(), 1. / 3.5 * tolerance);
    }

    TEST(TestElementsInSeries, Clone)
    {
        //Create
        auto el = ElementsInSeries::create();

        //Fill in
        el->emplace_back<LinearSpring>(2.);
        el->emplace_back<LinearSpring>(1.);
        el->emplace_back<LinearSpring>(0.5);

        auto clone = el->clone();
        ASSERT_TRUE(clone);
        EXPECT_NEAR(1. / 3.5, clone->stiffness(), 1. / 3.5 * tolerance);

        //Clone can be typecasted back
        auto clone_casted = std::dynamic_pointer_cast<ElementsInSeries>(clone);
        ASSERT_TRUE(clone_casted);
        EXPECT_NEAR(1. / 3.5, clone_casted->stiffness(), 1. / 3.5 * tolerance);

        //Check addresses are different for elements
        auto it_ref = el->cbegin();
        for(auto it = clone_casted->cbegin(); it != clone_casted->cend(); ++it) {
            ASSERT_NE(it_ref, el->cend());
            EXPECT_NE(it_ref.operator->(), it.operator->());
            ++it_ref;
        }
        EXPECT_EQ(it_ref, el->cend());

        //Clear does not affect clone
        el->clear();
        EXPECT_NEAR(1. / 3.5, clone->stiffness(), 1. / 3.5 * tolerance);
    }

    TEST(TestElementsInSeries, Nontrivial)
    {
        //Create
        auto a = ElementsInSeries::create();

        //Top level
        auto b = a->emplace_back<ElementsInSeries>();
        a->emplace_back<LinearSpring>(1.);
        auto c = a->emplace_back<ElementsInSeries>();

        //First level
        b->emplace_back<LinearSpring>(2.);
        b->emplace_back<LinearSpring>(4.);

        c->emplace_back<LinearSpring>(0.5);
        auto d = c->emplace_back<ElementsInSeries>();
        c->emplace_back<LinearSpring>(0.25);

        //Second level
        d->emplace_back<LinearSpring>(0.125);

        EXPECT_NEAR(0.125, d->stiffness(), 0.125 * tolerance);
        EXPECT_NEAR(1. / 14., c->stiffness(), 1. / 14. * tolerance);
        EXPECT_NEAR(4. / 3., b->stiffness(), 4. / 3. * tolerance);
        EXPECT_NEAR(4. / 63., a->stiffness(), 4. / 63. * tolerance);
    }

    TEST(TestElementsInSeries, EmptyInchain)
    {
        //Setup
        auto a = ElementsInSeries::create();

        //One spring and second set of elements in series
        a->emplace_back<ElementsInSeries>();
        a->emplace_back<LinearSpring>(1.);

        EXPECT_NEAR(1., a->stiffness(), tolerance);
    };
}
