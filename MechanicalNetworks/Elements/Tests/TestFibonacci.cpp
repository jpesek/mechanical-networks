#include <cmath>
#include <gtest/gtest.h>

#include "MechanicalNetworks/Elements/ElementsInParallel.h"
#include "MechanicalNetworks/Elements/ElementsInSeries.h"
#include "MechanicalNetworks/Elements/LinearSpring.h"

namespace MechanicalNetworks::Elements::Tests {
    constexpr double tolerance = 1e-5;

    //Prepare the function
    //In test we will never use negative values.
    auto fib(long n) -> long
    {
        long i = 1;
        long j = 1;

        using std::swap;
        while((--n) > 0) {
            i += j;
            swap(i, j);
        }

        return j;
    }

    TEST(TestFibonacci, CheckSequence)
    {
        // Nontrivial combined test:
        // Imagine following "circuit" with springs of stiffness 1:
        //            +---/\/\---+
        //            |          |
        // +---/\/\---+          +---+
        //            |          |
        //            +--- RR ---+
        // Where for RR we recursively place the same "curcuit" up to level N.
        // When RR is just a single spring level N=1.
        // The effective stiffness of this setup is: k = F_{2N} / F_{2N+1}
        // For N->infty it converges to inverse of golden ratio.

        //First element
        std::shared_ptr<ElementBase> circuit = LinearSpring::create(1);
        //Note: Don't overdo it with level Fibonacci numbers grow at least as phi^{n-2} ~ 1.6^{n-2}
        for(long level = 1; level < 32; ++level) {
            //Serial part
            auto new_circuit = ElementsInSeries::create();

            new_circuit->emplace_back<LinearSpring>(1);
            auto fork = new_circuit->emplace_back<ElementsInParallel>();

            //Fill the fork
            fork->emplace_back<LinearSpring>(1);
            fork->push_back(circuit);

            //Replace original
            circuit = new_circuit;

            //Check value
            auto ref = double(fib(2 * level)) / double(fib(2 * level + 1));
            EXPECT_NEAR(ref, circuit->stiffness(), ref * tolerance);
        }
    }
}
