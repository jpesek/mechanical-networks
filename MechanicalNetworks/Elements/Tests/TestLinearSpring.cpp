#include <gtest/gtest.h>

#include "MechanicalNetworks/Elements/LinearSpring.h"

//! Namespace containing tests for the elements
namespace MechanicalNetworks::Elements::Tests {
    constexpr double tolerance = 1e-5;

    TEST(TestLinearSpring, Initialization)
    {
        //Create new spring
        auto spring = LinearSpring::create(5.);
        ASSERT_TRUE(spring);
        EXPECT_NEAR(5, spring->stiffness(), 5 * tolerance);

        //Try invalid values
        EXPECT_THROW(LinearSpring::create(-1.), Exceptions::InvalidArgument);

        //Overwrite value
        spring = LinearSpring::create(0.);
        EXPECT_NEAR(0, spring->stiffness(), tolerance);
    }

    TEST(TestLinearSpring, LateBinding)
    {
        //Store element as pointer to base
        std::shared_ptr<ElementBase> spring = LinearSpring::create(7.);
        ASSERT_TRUE(spring);
        EXPECT_NEAR(7, spring->stiffness(), 7 * tolerance);
    }

    TEST(TestLinearSpring, Clone)
    {
        //Create new spring
        auto spring = LinearSpring::create(6.);
        ASSERT_TRUE(spring);

        //Clone it
        auto clone = spring->clone();
        ASSERT_TRUE(clone);
        EXPECT_NEAR(6, clone->stiffness(), 6 * tolerance);
    }
}
