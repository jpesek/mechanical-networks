#ifndef MN_LINEARSPRING_H
#define MN_LINEARSPRING_H

#include "MechanicalNetworks/Exceptions/Exceptions.h"

#include "ElementFactory.h"

namespace MechanicalNetworks::Elements {
    /*! \brief Element representing a linear spring
     *
     * Element representing a linear spring with a given stiffness provided inside a constructor.
     */
    class LinearSpring final: public ElementFactory<LinearSpring>
    {
      public:
        /*! \brief Constructor
         *
         * Initializes k with stiffness.
         *
         * \param token ensures construction is only via create() method.
         * \param stiffness is the stiffness of this spring.
         * \throws Exceptions::InvalidArgument is stiffness is not positive.
         */
        LinearSpring(Token token, double stiffness);

        /*! \brief Returns copy of this element
         *
         * Copies the value of k to a new element
         *
         * \returns new instance of LinearSpring with the same stiffness
         */
        auto clone() const -> std::shared_ptr<ElementBase> override;

        /*! \brief Returns the stiffness
         *
         * \returns k
         */
        auto stiffness() const -> double override;

      private:
        //! Variable holding our stiffness.
        double k_{ 0. };
    };
}
#endif
