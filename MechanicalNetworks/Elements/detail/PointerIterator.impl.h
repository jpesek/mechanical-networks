#ifndef MN_POINTERITERATOR_IMPL_H
#define MN_POINTERITERATOR_IMPL_H

#include "PointerIterator.h"

namespace MechanicalNetworks::Elements::detail {
    template<class It>
    PointerIterator<It>::PointerIterator(It it)
        : it_{ std::move(it) }
    {
    }

    template<class It>
    auto PointerIterator<It>::operator++() -> PointerIterator&
    {
        ++it_;
        return *this;
    }

    template<class It>
    auto PointerIterator<It>::operator++(int) -> PointerIterator
    {
        auto orig = *this;
        ++it_;
        return orig;
    }

    template<class It>
    auto PointerIterator<It>::operator--() -> PointerIterator&
    {
        --it_;
        return *this;
    }

    template<class It>
    auto PointerIterator<It>::operator--(int) -> PointerIterator
    {
        auto orig = *this;
        --it_;
        return orig;
    }

    template<class It>
    auto PointerIterator<It>::operator==(const PointerIterator& other) const -> bool
    {
        return this->it_ == other.it_;
    }

    template<class It>
    auto PointerIterator<It>::operator!=(const PointerIterator& other) const -> bool
    {
        return ! this->operator==(other);
    }

    template<class It>
    auto PointerIterator<It>::operator*() const -> reference
    {
        if(auto ptr = *it_) {
            return *ptr;
        }

        throw Exceptions::InvalidValue("PointerIterator encountered nullptr as a value. Cannot dereference.");
    }

    template<class It>
    auto PointerIterator<It>::operator->() const -> pointer
    {
        return it_->get();
    }
}
#endif
