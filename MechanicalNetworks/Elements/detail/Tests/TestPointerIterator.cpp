#include <gtest/gtest.h>
#include <memory>
#include <vector>

#include "MechanicalNetworks/Elements/detail/PointerIterator.h"

//! Namespace containing tests for detail
namespace MechanicalNetworks::Elements::detail::Tests {
    //Prepare the pointer type for tests
    using pointer_type = std::shared_ptr<int>;

    //Prepare storage type
    using storage_type = std::vector<pointer_type>;

    //And finally prepare iterator type
    using iterator_type = PointerIterator<typename storage_type::iterator>;

    TEST(TestPointerIterator, Initialization)
    {
        //Initialize storage
        storage_type storage{};
        storage.push_back(std::make_shared<int>(5));
        storage.push_back(std::make_shared<int>(4));
        storage.push_back(std::make_shared<int>(3));

        //Prepare empty
        iterator_type empty;
        EXPECT_EQ(empty, empty);

        //Initialize with begin()
        iterator_type beg{ storage.begin() };
        EXPECT_EQ(beg, beg);
        EXPECT_NE(empty, beg);

        EXPECT_EQ(5, *beg);

        //Initialize with begin()+1
        iterator_type next{ ++storage.begin() };
        EXPECT_EQ(next, next);
        EXPECT_NE(beg, next);
        EXPECT_NE(empty, next);

        EXPECT_EQ(4, *next);
    }

    TEST(TestPointerIterator, Increments)
    {
        //Initialize storage
        storage_type storage{};
        storage.push_back(std::make_shared<int>(5));
        storage.push_back(std::make_shared<int>(4));
        storage.push_back(std::make_shared<int>(3));

        //Initialize with begin()
        iterator_type it = storage.begin();
        EXPECT_EQ(5, *it);

        //Prefix increment
        auto pref = ++it;
        //They should be equal
        EXPECT_EQ(pref, it);

        //Have same value
        EXPECT_EQ(4, *it);
        EXPECT_EQ(4, *pref);

        //Point to the same spot
        EXPECT_EQ(it.operator->(), pref.operator->());

        //Prefix increment
        auto post = it++;
        //They should be equal
        EXPECT_NE(post, it);
        EXPECT_EQ(pref, post);

        //Have same value
        EXPECT_EQ(3, *it);
        EXPECT_EQ(4, *post);

        //Point to the same spot
        EXPECT_NE(it.operator->(), post.operator->());
        EXPECT_EQ(pref.operator->(), post.operator->());
    }

    TEST(TestPointerIterator, Decrements)
    {
        //Initialize storage
        storage_type storage{};
        storage.push_back(std::make_shared<int>(5));
        storage.push_back(std::make_shared<int>(4));
        storage.push_back(std::make_shared<int>(3));

        //Initialize with begin()
        iterator_type it{ --storage.end() };
        EXPECT_EQ(3, *it);

        //Prefix increment
        auto pref = --it;
        //They should be equal
        EXPECT_EQ(pref, it);

        //Have same value
        EXPECT_EQ(4, *it);
        EXPECT_EQ(4, *pref);

        //Point to the same spot
        EXPECT_EQ(it.operator->(), pref.operator->());

        //Prefix increment
        auto post = it--;
        //They should be equal
        EXPECT_NE(post, it);
        EXPECT_EQ(pref, post);

        //Have same value
        EXPECT_EQ(5, *it);
        EXPECT_EQ(4, *post);

        //Point to the same spot
        EXPECT_NE(it.operator->(), post.operator->());
        EXPECT_EQ(pref.operator->(), post.operator->());
    }

    TEST(TestPointerIterator, Loop)
    {
        //Initialize storage
        storage_type storage{};
        storage.push_back(std::make_shared<int>(5));
        storage.push_back(std::make_shared<int>(4));
        storage.push_back(std::make_shared<int>(3));

        //Reference
        const std::vector<int> expected_values{ 5, 4, 3 };
        auto it_ref = expected_values.cbegin();

        //Test simple loop
        for(iterator_type it = storage.begin(); it != storage.end(); ++it) {
            ASSERT_NE(it_ref, expected_values.cend());
            EXPECT_EQ(*it_ref, *it);
            ++it_ref;
        }
        EXPECT_EQ(it_ref, expected_values.cend());
    }

    TEST(TestPointerIterator, Failure)
    {
        //Initialize storage
        storage_type storage{};
        storage.push_back(nullptr);
        storage.push_back(std::make_shared<int>(4));
        storage.push_back(std::make_shared<int>(3));

        iterator_type it{ storage.begin() };
        EXPECT_THROW(*it, Exceptions::InvalidValue);
        EXPECT_FALSE(it.operator->());
    }
}
