#ifndef MN_POINTERITERATOR_H
#define MN_POINTERITERATOR_H

#include "MechanicalNetworks/Exceptions/Exceptions.h"

/*! \brief Namespace for storing technical details.
 *
 * \warning Elements from this namespace are not intended to be directly used by users.
 */
namespace MechanicalNetworks::Elements::detail {
    /*! \brief Iterator automatically dereferencing pointers
     *
     * Adapter to standard iterator over container of pointers (either shared or unique),
     * overloads operator*() and operator->() to dereference the pointer.
     *
     * \tparam It is the underlying iterator type.
     */
    template<class It>
    class PointerIterator
    {
      public:
        //! Inherit difference type
        using difference_type = typename It::difference_type;
        //! Value type is the actual element_type of the pointer
        using value_type = typename It::value_type::element_type;
        //! Reference type is reference to value
        using reference = value_type&;
        //! Pointer type is pointer to value
        using pointer = value_type*;
        //! Iterator category is bi-directional iterator
        using iterator_category = std::bidirectional_iterator_tag;

        //! Default constructor
        PointerIterator() = default;

        /*! \brief Constructor with initialization
         *
         * \param it is the iterator we initialize out value with.
         */
        PointerIterator(It it);

        //! Defaulted copy constructor
        PointerIterator(const PointerIterator&) = default;

        //! Defaulted move constructor
        PointerIterator(PointerIterator&&) noexcept = default;

        //! Defaulted copy assignment
        auto operator=(const PointerIterator&) -> PointerIterator& = default;

        //! Defaulted move assignment
        auto operator=(PointerIterator&&) noexcept -> PointerIterator& = default;

        //! Defaulted destructor
        ~PointerIterator() = default;

        //! Prefix increment
        auto operator++() -> PointerIterator&;

        //! Postfix increment
        auto operator++(int) -> PointerIterator;

        //! Prefix decrement
        auto operator--() -> PointerIterator&;

        //! Postfix decrement
        auto operator--(int) -> PointerIterator;

        /*! \brief Equality comparison
         *
         * \param other is the iterator we compare with
         * \returns this->it_ == other.it_;
         */
        auto operator==(const PointerIterator& other) const -> bool;

        /*! \brief Inequality comparison
         *
         * \param other is the iterator we compare with
         * \returns this->it_ != other.it_;
         */
        auto operator!=(const PointerIterator& other) const -> bool;

        /*! \brief Dereference the iterator
         *
         * \returns *(*it_);
         * \throws Exceptions::InvalidValue is *it_ is nullptr.
         *
         * \warning Dereferencing iterator beyond end is ill-defined.
         */
        auto operator*() const -> reference;

        /*! \brief Returns pointer to value
         *
         * \returns it_->get();
         * \throws Exceptions::InvalidValue is *it_ is nullptr.
         */
        auto operator->() const -> pointer;

      private:
        //! Iterator we own
        It it_{};
    };
}
#include "PointerIterator.impl.h"
#endif
