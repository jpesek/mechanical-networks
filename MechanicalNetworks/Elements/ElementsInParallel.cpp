#include <numeric>

#include "ElementsInParallel.h"

namespace MechanicalNetworks::Elements {
    auto ElementsInParallel::clone() const -> std::shared_ptr<ElementBase>
    {
        auto result = ElementsInParallel::create();
        if(! result) {
            throw Exceptions::InitializationError("Memory allocation error in ElementsInParallel::create()");
        }

        for(const auto& el: *this) {
            result->push_back(el.clone());
        }

        return result;
    }

    auto ElementsInParallel::stiffness() const -> double
    {
        //Are we empty?
        if(! (*this)) {
            //Return infinity
            return std::numeric_limits<double>::infinity();
        }

        return std::reduce(this->begin(), this->end(), 0.);
    }
}
