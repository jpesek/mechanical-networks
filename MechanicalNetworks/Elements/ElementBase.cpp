#include "ElementBase.h"

namespace MechanicalNetworks::Elements {
    ElementBase::ElementBase([[maybe_unused]] Token token)
    {
    }

    ElementBase::operator double() const
    {
        return this->stiffness();
    }
}
