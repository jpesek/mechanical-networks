#include <numeric>

#include "ElementsInSeries.h"

namespace MechanicalNetworks::Elements {
    auto ElementsInSeries::clone() const -> std::shared_ptr<ElementBase>
    {
        auto result = ElementsInSeries::create();
        if(! result) {
            throw Exceptions::InitializationError("Memory allocation error in ElementsInSeries::create()");
        }

        for(const auto& el: *this) {
            result->push_back(el.clone());
        }

        return result;
    }

    auto ElementsInSeries::stiffness() const -> double
    {
        //Are we empty?
        if(! (*this)) {
            //Return infinity
            return std::numeric_limits<double>::infinity();
        }

        auto inv_k = 0.;
        for(const auto& el: *this) {
            inv_k += 1. / el.stiffness();
        }

        return 1. / inv_k;
    }
}
