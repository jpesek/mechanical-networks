#ifndef MN_ELEMENTFACTORY_H
#define MN_ELEMENTFACTORY_H

#include <memory>

#include "ElementBase.h"

namespace MechanicalNetworks::Elements {
    /*! \brief Define a factory
     *
     * Used in CRTP to provide automatically a create() method for all Elements.
     *
     * \tparam T is the final class,
     *         i.e. class inheriting from this ElementFactory to which we can statically cast this.
     * \tparam Base is the base class.
     *         By default it is set to ElementBase.
     */
    template<class T, class Base = ElementBase>
    class ElementFactory: public Base
    {
      public:
        //! Inherit constructors
        using Base::Base;

        //! Define base
        using base = ElementFactory;

        /*! \brief Create factory static method
         *
         * Creates a new instance of T, while passing instance of Token
         * and additional args to the constructor.
         *
         * \tparam Args are types of arguments of the constructor
         * \param args are additional parameters to the constructor of T
         * \returns Shared pointer with a new instance of T
         */
        template<class... Args>
        static auto create(Args&&... args) -> std::shared_ptr<T>;
    };
}
#include "ElementFactory.impl.h"
#endif
