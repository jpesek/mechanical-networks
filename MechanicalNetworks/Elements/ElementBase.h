#ifndef MN_ELEMENTBASE_H
#define MN_ELEMENTBASE_H

#include <memory>

/*! \brief Contains all essential functionality for Mechanical networks
 *
 * In particular:
 * * Elements contains definitions and declaration of individual mechanical elements.
 * * IO contains functions and methods for parsing inputs representing networks to their corresponding representation.
 */
namespace MechanicalNetworks {
    /*! \brief Contains definitions and declarations of individual mechanical elements
     *
     * In particular we define:
     * * ElementBase - an abstract base specifying all the elements
     * * LinearSpring - an element representing a linear spring
     * * ElementsInSeries - an element representing set of elements connected in series
     * * ElementsInParallel - an element representing set of elements connected in parallel
     */
    namespace Elements {
        /*! \brief Define the common base for all elements.
         *
         * Define a public interface `stiffness()` returning the equivalent stiffness of this mechanical element.
         * Moreover defines protected Token necessary to prevent user to directly creating these elements.
         */
        class ElementBase
        {
          protected:
            /*! \brief Token
             *
             * As it is protected it can be only created within the scope of the ElementBase or any of its ancestors.
             */
            class Token
            {
              public:
                //! Defaulted default constructor
                Token() = default;

                /*! \brief Deleted copy constructor
                 *
                 * Prevents copy of the token out of scope.
                 */
                Token(const Token&) = delete;

                //! Defaulted move constructor
                Token(Token&&) noexcept = default;

                //! Deleted copy assignment
                auto operator=(const Token&) -> Token& = delete;

                //! Defaulted move assignment
                auto operator=(Token&&) -> Token& = default;

                //! Defaulted destructor
                ~Token() = default;
            };

          public:
            /*! \brief Constructor
             *
             * Uses Token to limit construction only within the context of this class or its children.
             *
             * \param token is a token proving we are within the context.
             *
             * \sa ElementFactory
             */
            ElementBase([[maybe_unused]] Token token);

            /*! \brief Creates a deep copy of the element
             *
             * This method provides a safe version of copy constructor and replaces protected copy constructors.
             * See C.130 from CppCoreGuidelines.
             *
             * \returns deep copy of this element
             */
            virtual auto clone() const -> std::shared_ptr<ElementBase> = 0;

            //! Defaulted destructor
            virtual ~ElementBase() noexcept = default;

            /*! \brief Returns a stiffness of this element
             *
             * \returns equivalent stiffness of this element
             */
            virtual auto stiffness() const -> double = 0;

            /*! \brief Shorthand for stiffness
             *
             * \returns this->stiffness()
             */
            operator double() const;

          protected:
            //! Defaulted copy constructor
            ElementBase(const ElementBase&) = default;

            //! Defaulted move constructor
            ElementBase(ElementBase&&) noexcept = default;

            //! Defaulted copy assignment operator
            auto operator=(const ElementBase&) -> ElementBase& = default;

            //! Defaulted move assignment operator
            auto operator=(ElementBase&&) noexcept -> ElementBase& = default;
        };
    }
}
#endif
