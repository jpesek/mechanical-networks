#include "ElementStorage.h"

namespace MechanicalNetworks::Elements {
    void ElementStorage::push_back(const std::shared_ptr<ElementBase>& el)
    {
        if(! el) {
            throw Exceptions::InvalidArgument("push_back() in ElementStorage encountered nullptr");
        }

        storage_.push_back(el);
    }

    void ElementStorage::push_back(std::shared_ptr<ElementBase>&& el)
    {
        if(! el) {
            throw Exceptions::InvalidArgument("push_back() in ElementStorage encountered nullptr");
        }

        storage_.push_back(std::move(el));
    }

    void ElementStorage::clear()
    {
        storage_.clear();
    }

    auto ElementStorage::size() const -> std::size_t
    {
        return storage_.size();
    }

    ElementStorage::operator bool() const
    {
        return size() > 0;
    }

    auto ElementStorage::begin() -> iterator
    {
        return storage_.begin();
    }

    auto ElementStorage::begin() const -> const_iterator
    {
        return cbegin();
    }

    auto ElementStorage::cbegin() const -> const_iterator
    {
        return storage_.cbegin();
    }

    auto ElementStorage::end() -> iterator
    {
        return storage_.end();
    }

    auto ElementStorage::end() const -> const_iterator
    {
        return cend();
    }

    auto ElementStorage::cend() const -> const_iterator
    {
        return storage_.cend();
    }

    auto ElementStorage::rbegin() -> reverse_iterator
    {
        return reverse_iterator{ storage_.end() };
    }

    auto ElementStorage::rbegin() const -> const_reverse_iterator
    {
        return crbegin();
    }

    auto ElementStorage::crbegin() const -> const_reverse_iterator
    {
        return const_reverse_iterator{ storage_.cend() };
    }

    auto ElementStorage::rend() -> reverse_iterator
    {
        return reverse_iterator{ storage_.begin() };
    }

    auto ElementStorage::rend() const -> const_reverse_iterator
    {
        return crend();
    }

    auto ElementStorage::crend() const -> const_reverse_iterator
    {
        return const_reverse_iterator{ storage_.cbegin() };
    }
}
