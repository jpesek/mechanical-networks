#include <cmath>
#include <gtest/gtest.h>

#include "MechanicalNetworks/stiffness.h"

//! Namespace with tests for stiffness()
namespace MechanicalNetworks::IO::Tests {
    constexpr double tolerance = 1e-5;

    TEST(TestStiffness, Springs)
    {
        //Try couple numbers:
        // - int:
        EXPECT_NEAR(1., stiffness("1"), tolerance);

        // - float:
        EXPECT_NEAR(1.5, stiffness("1.5"), 1.5 * tolerance);

        // - missing leading number:
        EXPECT_NEAR(0.5, stiffness(".5"), 0.5 * tolerance);

        // - scientific:
        EXPECT_NEAR(1.15e-2, stiffness("1.15e-2"), 1.15e-2 * tolerance);

        // - leading space:
        EXPECT_NEAR(1.15e-2, stiffness("  1.15e-2"), 1.15e-2 * tolerance);

        // - trailing space:
        EXPECT_NEAR(1.15e-2, stiffness("  1.15e-2   "), 1.15e-2 * tolerance);

        // - empty
        EXPECT_TRUE(std::isinf(stiffness("     ")));

        // - negative stiffness
        EXPECT_THROW(stiffness("  -1.15e-2   "), Exceptions::InvalidArgument);
        // - trailing comma
        EXPECT_THROW(stiffness("  1.5,  "), Exceptions::ParsingError);
        // - not a number
        EXPECT_THROW(stiffness(" str  "), Exceptions::ParsingError);
        // - not a number
        EXPECT_THROW(stiffness(" 1.5str  "), Exceptions::ParsingError);
    }

    TEST(TestStiffness, Series)
    {
        //Try couple values:
        // - empty
        EXPECT_TRUE(std::isinf(stiffness("()")));

        // - 1 element
        EXPECT_NEAR(1.5, stiffness("(1.5)"), 1.5 * tolerance);

        // - more elements
        EXPECT_NEAR(1., stiffness("(.2e1,30.0e-1,6)"), tolerance);

        // - chained
        EXPECT_NEAR(1., stiffness("((4,4),(9,9,9),6)"), tolerance);

        // - missing opening bracked
        EXPECT_THROW(stiffness("2,3,6)"), Exceptions::ParsingError);
        // - missing closing bracked
        EXPECT_THROW(stiffness("(2,3,6"), Exceptions::ParsingError);
        // - wrong closing bracked
        EXPECT_THROW(stiffness("(2,3,6]"), Exceptions::ParsingError);
        // - premature end
        EXPECT_THROW(stiffness("(2,3),6"), Exceptions::ParsingError);
        // - extra bracket
        EXPECT_THROW(stiffness("(2,3,6))"), Exceptions::ParsingError);
    }

    TEST(TestStiffness, Parallel)
    {
        //Try couple values:
        // - empty
        EXPECT_TRUE(std::isinf(stiffness("[]")));

        // - 1 element
        EXPECT_NEAR(1.5, stiffness("[1.5]"), 1.5 * tolerance);

        // - more elements
        EXPECT_NEAR(11., stiffness("[.2e1,30.0e-1,6]"), 11. * tolerance);

        // - chained
        EXPECT_NEAR(11., stiffness("[[1,1],[0.5,1.5,1],6]"), 11. * tolerance);

        // - missing opening bracked
        EXPECT_THROW(stiffness("2,3,6]"), Exceptions::ParsingError);
        // - missing closing bracked
        EXPECT_THROW(stiffness("[2,3,6"), Exceptions::ParsingError);
        // - wrong closing bracked
        EXPECT_THROW(stiffness("[2,3,6)"), Exceptions::ParsingError);
        // - premature end
        EXPECT_THROW(stiffness("[2,3],6"), Exceptions::ParsingError);
        // - extra bracket
        EXPECT_THROW(stiffness("[2,3,6]]"), Exceptions::ParsingError);
    }

    TEST(TestStiffness, Mixed)
    {
        //Try couple values:
        EXPECT_NEAR(50. / 6., stiffness("(10,[20,30])"), 50. / 6. * tolerance);
        EXPECT_NEAR(22., stiffness("[10,(20,30)]"), 22. * tolerance);

        //Fibonacci:
        EXPECT_NEAR(5. / 8., stiffness("(1,[1,(1,[1,1])])"), 5. / 8. * tolerance);
        EXPECT_NEAR(13. / 21., stiffness("(1,[1,(1,[1,(1,[1,1])])])"), 13. / 21. * tolerance);

        // - mixed brackets
        EXPECT_THROW(stiffness("[2,(3,6],5)"), Exceptions::ParsingError);
        // - extra brackets
        EXPECT_THROW(stiffness("[2,(3,6],5)]"), Exceptions::ParsingError);
        // - missing brackets
        EXPECT_THROW(stiffness("[2,(3,6,5]"), Exceptions::ParsingError);
        // - missing token
        EXPECT_THROW(stiffness("[2,(3,,5]"), Exceptions::ParsingError);
    }
}
