#include "stiffness.h"
#include "IO/parse.h"

namespace MechanicalNetworks {
    auto stiffness(const std::string& str) -> double
    {
        auto repr = IO::parse(str);
        if(! repr) {
            throw Exceptions::ParsingError("Parsing failed and returned nullptr.");
        }

        return repr->stiffness();
    }
}
