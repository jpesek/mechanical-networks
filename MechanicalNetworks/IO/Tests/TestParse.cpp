#include <cmath>
#include <gtest/gtest.h>

#include "MechanicalNetworks/IO/parse.h"

//! Namespace with tests for parse()
namespace MechanicalNetworks::IO::Tests {
    constexpr double tolerance = 1e-5;

    TEST(TestParse, Springs)
    {
        //Try couple numbers:
        {
            // - int:
            auto result = parse("1");
            ASSERT_TRUE(result);
            EXPECT_NEAR(1., *result, tolerance);
        }

        {
            // - float:
            auto result = parse("1.5");
            ASSERT_TRUE(result);
            EXPECT_NEAR(1.5, *result, 1.5 * tolerance);
        }

        {
            // - missing leading number:
            auto result = parse(".5");
            ASSERT_TRUE(result);
            EXPECT_NEAR(0.5, *result, 0.5 * tolerance);
        }

        {
            // - scientific:
            auto result = parse("1.15e-2");
            ASSERT_TRUE(result);
            EXPECT_NEAR(1.15e-2, *result, 1.15e-2 * tolerance);
        }

        {
            // - leading space:
            auto result = parse("  1.15e-2");
            ASSERT_TRUE(result);
            EXPECT_NEAR(1.15e-2, *result, 1.15e-2 * tolerance);
        }

        {
            // - trailing space:
            auto result = parse("  1.15e-2   ");
            ASSERT_TRUE(result);
            EXPECT_NEAR(1.15e-2, *result, 1.15e-2 * tolerance);
        }

        {
            // - empty
            auto result = parse("     ");
            ASSERT_TRUE(result);
            EXPECT_TRUE(std::isinf(*result));
        }

        // - negative stiffness
        EXPECT_THROW(parse("  -1.15e-2   "), Exceptions::InvalidArgument);
        // - trailing comma
        EXPECT_THROW(parse("  1.5,  "), Exceptions::ParsingError);
        // - not a number
        EXPECT_THROW(parse(" str  "), Exceptions::ParsingError);
        // - not a number
        EXPECT_THROW(parse(" 1.5str  "), Exceptions::ParsingError);
    }

    TEST(TestParse, Series)
    {
        //Try couple values:
        {
            // - empty
            auto result = parse("()");
            ASSERT_TRUE(result);
            EXPECT_TRUE(std::isinf(*result));
        }

        {
            // - 1 element
            auto result = parse("(1.5)");
            ASSERT_TRUE(result);
            EXPECT_NEAR(1.5, *result, 1.5 * tolerance);
        }

        {
            // - more elements
            auto result = parse("(.2e1,30.0e-1,6)");
            ASSERT_TRUE(result);
            EXPECT_NEAR(1., *result, tolerance);
        }

        {
            // - chained
            auto result = parse("((4,4),(9,9,9),6)");
            ASSERT_TRUE(result);
            EXPECT_NEAR(1., *result, tolerance);
        }

        // - missing opening bracked
        EXPECT_THROW(parse("2,3,6)"), Exceptions::ParsingError);
        // - missing closing bracked
        EXPECT_THROW(parse("(2,3,6"), Exceptions::ParsingError);
        // - wrong closing bracked
        EXPECT_THROW(parse("(2,3,6]"), Exceptions::ParsingError);
        // - premature end
        EXPECT_THROW(parse("(2,3),6"), Exceptions::ParsingError);
        // - extra bracket
        EXPECT_THROW(parse("(2,3,6))"), Exceptions::ParsingError);
    }

    TEST(TestParse, Parallel)
    {
        //Try couple values:
        {
            // - empty
            auto result = parse("[]");
            ASSERT_TRUE(result);
            EXPECT_TRUE(std::isinf(*result));
        }

        {
            // - 1 element
            auto result = parse("[1.5]");
            ASSERT_TRUE(result);
            EXPECT_NEAR(1.5, *result, 1.5 * tolerance);
        }

        {
            // - more elements
            auto result = parse("[.2e1,30.0e-1,6]");
            ASSERT_TRUE(result);
            EXPECT_NEAR(11., *result, 11. * tolerance);
        }

        {
            // - chained
            auto result = parse("[[1,1],[0.5,1.5,1],6]");
            ASSERT_TRUE(result);
            EXPECT_NEAR(11., *result, 11. * tolerance);
        }

        // - missing opening bracked
        EXPECT_THROW(parse("2,3,6]"), Exceptions::ParsingError);
        // - missing closing bracked
        EXPECT_THROW(parse("[2,3,6"), Exceptions::ParsingError);
        // - wrong closing bracked
        EXPECT_THROW(parse("[2,3,6)"), Exceptions::ParsingError);
        // - premature end
        EXPECT_THROW(parse("[2,3],6"), Exceptions::ParsingError);
        // - extra bracket
        EXPECT_THROW(parse("[2,3,6]]"), Exceptions::ParsingError);
    }

    TEST(TestParse, Mixed)
    {
        //Try couple values:
        {
            auto result = parse("(10,[20,30])");
            ASSERT_TRUE(result);
            EXPECT_NEAR(50. / 6., *result, 50. / 6. * tolerance);
        }

        {
            auto result = parse("[10,(20,30)]");
            ASSERT_TRUE(result);
            EXPECT_NEAR(22., *result, 22. * tolerance);
        }

        //Fibonacci:
        {
            auto result = parse("(1,[1,(1,[1,1])])");
            ASSERT_TRUE(result);
            EXPECT_NEAR(5. / 8., *result, 5. / 8. * tolerance);
        }

        {
            auto result = parse("(1,[1,(1,[1,(1,[1,1])])])");
            ASSERT_TRUE(result);
            EXPECT_NEAR(13. / 21., *result, 13. / 21. * tolerance);
        }

        // - mixed brackets
        EXPECT_THROW(parse("[2,(3,6],5)"), Exceptions::ParsingError);
        // - extra brackets
        EXPECT_THROW(parse("[2,(3,6],5)]"), Exceptions::ParsingError);
        // - missing brackets
        EXPECT_THROW(parse("[2,(3,6,5]"), Exceptions::ParsingError);
        // - missing token
        EXPECT_THROW(parse("[2,(3,,5]"), Exceptions::ParsingError);
    }
}
