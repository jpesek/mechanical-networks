#ifndef MN_PARSE_H
#define MN_PARSE_H

#include <string>

#include "MechanicalNetworks/Elements/ElementBase.h"
#include "MechanicalNetworks/Exceptions/Exceptions.h"

/*! \brief Namespace containing I/O methods.
 *
 * At this point mainly parse() for parsing the input.
 */
namespace MechanicalNetworks::IO {
    /*! \brief Parse the string and provide an object representation of the given stream.
     *
     * Assumptions:
     *  * String represents a single circuit.
     *
     * Parsing rules:
     *  * Any white space characters (`' '`,`'\\r'`,`'\\n'`,`'\\t'`) are ommited.
     *  * Tokens '[' and '(' correspond elements in parallel and series respectivelly.
     *  * Token separator are ',',']',')' and the opening brackets.
     *  * Closing brackets must match opening brackets.
     *  * Any other token is interpreted as a number.
     *  * Empty string is interpreted as spring with stiffness infty.
     *
     * \param str is input string which we will parse
     * \returns Elements representing the input string.
     * \throws Exceptions::ParsingError whenever ill formatted input is met.
     * \throws Exceptions::InvalidArgument whenever spring with negative stiffness is met.
     */
    auto parse(const std::string& str) -> std::shared_ptr<Elements::ElementBase>;
}
#endif
