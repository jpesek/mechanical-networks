#include <iostream>
#include <iterator>
#include <limits>
#include <stack>

#include "MechanicalNetworks/Elements/ElementsInParallel.h"
#include "MechanicalNetworks/Elements/ElementsInSeries.h"
#include "MechanicalNetworks/Elements/LinearSpring.h"
#include "MechanicalNetworks/Exceptions/Exceptions.h"

#include "parse.h"

namespace MechanicalNetworks::IO {
    /*! \brief Helper function - test whether we have white space
     *
     * Test whether character c is whitespace.
     * In particular: space, new line, carriage return or tab.
     *
     * \param c is character we test.
     * \returns true if c is whitespace
     */
    auto is_whitespace(const char& c) -> bool
    {
        switch(c) {
        case ' ':
            [[fallthrough]];
        case '\r':
            [[fallthrough]];
        case '\t':
            [[fallthrough]];
        case '\n':
            return true;
        default:
            return false;
        }
    }

    /*! \brief Helper function - create storage
     *
     * Test whether character at it is one of opening character for storage.
     * If yes create corresponding storage,
     * advance iterator, and return storage together with terminating character.
     *
     * \param it is the iterator which value we inspect
     * \returns pair terminating character and storage
     *          or pair formed by 'X' and nullptr if not storage.
     *
     * \note If we provide new \link MechanicalNetworks::Elements::ElementStorage ElementStorage \endlink this is the only function which needs to be modified.
     */
    auto get_new_storage(std::string::const_iterator& it) -> std::pair<char, std::shared_ptr<MechanicalNetworks::Elements::ElementStorage>>
    {
        switch(*it) {
        case '(':
            ++it;
            return { ')', MechanicalNetworks::Elements::ElementsInSeries::create() };
        case '[':
            ++it;
            return { ']', MechanicalNetworks::Elements::ElementsInParallel::create() };
        default:
            return { 'X', nullptr };
        }
    }

    /*! \brief Parse the value of the spring
     *
     * Parse string from position start until it still resembles number.
     * Adjust start to point behind the data which were processed.
     * In case nothing can be processed throws std::invalid_argument
     *
     * \param start is starting point of processing.
     *        After execution the value is adjusted to point behind the processed string.
     * \param end is maximal range of processing
     * \returns new instance of \link MechanicalNetworks::Elements::LinearSpring LinearSpring \endlink with value from parsed.
     * \throws std::invalid_argument if nothing to process
     */
    auto get_spring(std::string::const_iterator& start, const std::string::const_iterator& end) -> std::shared_ptr<MechanicalNetworks::Elements::LinearSpring>

    {
        //Extract the rest as a token
        auto token = std::string{ start, end };

        std::size_t pos = 0;
        double value = std::stod(token, &pos);
        start += pos;

        return MechanicalNetworks::Elements::LinearSpring::create(value);
    }

    auto parse(const std::string& str) -> std::shared_ptr<Elements::ElementBase>
    {
        using namespace std::string_literals;
        using namespace MechanicalNetworks::Elements;

        //Start parsing
        auto token_start = str.begin();

        //Find first non-space character
        while((token_start != str.end())
              && is_whitespace(*token_start)) {
            ++token_start;
        }
        //Are we done?
        if(token_start == str.end()) {
            //Nothing to do -> return infty
            return LinearSpring::create(std::numeric_limits<double>::infinity());
        }

        //Do we start a container?
        auto result = get_new_storage(token_start);
        if(result.first == 'X') {
            //No - we have only a single value
            decltype(get_spring(token_start, str.end())) spring = nullptr;
            try {
                spring = get_spring(token_start, str.end());
            } catch(const std::invalid_argument&) {
                //We couldn't convert anything:
                throw Exceptions::ParsingError("Parsing error. String: '"s + std::string{ token_start, str.end() } + "' starting at position " + std::to_string(token_start - str.begin()) + " is not a number.");
            }

            //Find first non-space character
            while((token_start != str.end())
                  && is_whitespace(*token_start)) {
                ++token_start;
            }

            //Do we still have something at the end?
            if(token_start != str.end()) {

                throw Exceptions::ParsingError("Parsing error. Unexpected trailing characters: '" + std::string{ token_start, str.end() } + "' starting at position " + std::to_string(token_start - str.begin()));
            }

            //Return value
            return spring;
        }

        //Prepare stack for all possible levels
        //Currently with one level
        std::stack<std::pair<char, std::shared_ptr<ElementStorage>>> stack{ { result } };
        bool was_end = false;
        while(token_start != str.end()) {
            //Find first non-space character
            while((token_start != str.end())
                  && is_whitespace(*token_start)) {
                ++token_start;
            }
            //Are we done?
            if(token_start == str.end()) {
                break;
            }

            //Stack should not be empty at this point as we are processing another data
            if(stack.empty()) {
                throw Exceptions::ParsingError("Parsing error. No set open while we still need to process: '"s + std::string{ token_start, str.end() } + "' at position " + std::to_string(token_start - str.begin()) + " while no storage is open.");
            }

            //Comma can occur at two occasions
            if(*token_start == ',') {
                if(! was_end) {
                    throw Exceptions::ParsingError("Parsing error. Unexpected comma: '"s + std::string{ token_start, str.end() } + "' at position " + std::to_string(token_start - str.begin()) + " while no token has ended.");
                }

                ++token_start;
                continue;
            }
            was_end = false;

            //Are we closing current level?
            auto top = stack.top();
            if(*token_start == top.first) {
                //Get the top and pop it
                result = top;
                stack.pop();

                //Advance token and mark stuff we ended
                ++token_start;
                was_end = true;
                continue;
            }

            //Do we open new level?
            if(auto new_result = get_new_storage(token_start); new_result.second) {
                //Add it to the previous result end and push it to the stack
                top.second->push_back(new_result.second);
                stack.push(new_result);

                //Replace current top
                result = std::move(new_result);
                continue;
            }

            //Has to be value
            decltype(get_spring(token_start, str.end())) result = nullptr;
            try {
                result = get_spring(token_start, str.end());
            } catch(const std::invalid_argument&) {
                //We couldn't convert anything:
                throw Exceptions::ParsingError("Parsing error. String: '"s + std::string{ token_start, str.end() } + "' starting at position " + std::to_string(token_start - str.begin()) + " is not a number.");
            }

            //Add to the current status
            top.second->push_back(std::move(result));
            was_end = true;
        }

        //Stack should be now empty
        if(! stack.empty()) {
            throw Exceptions::ParsingError("Parsing error. There '" + std::to_string(stack.size()) + "' unterminated elements.");
        }

        return result.second;
    }
}
