# Mechanical networks
[This project](https://gitlab.inria.fr/jpesek/mechanical-networks) serves as a toy example to showcase a TDD workflow and various approaches to design.
For this reason we also setup project on gitlab including CI/CD pipelines, verifying various aspects of the code.

## Goal
The framework computes effective stiffness of mechanical "circuits" (0D/1D networks) composed from set of linear spring with anisotropic stiffness linked in series and parallel.
In particular, springs linked in parallel have effective stiffness given by
```math
k_\text{eff} = \sum\limits_i k_i,
```
while in series
```math
k_\text{eff} = \left[ \sum\limits_i \frac{1}{k_i} \right]^{-1}.
```

We provide couple essential components: 
 * Library `libMechanicalNetworks.so` providing all essential methods to link it in, see [documentation](https://jpesek.gitlabpages.inria.fr/mechanical-networks/).
 * `cmake` package to simplify linking to 3rd party projects
 * Simple binary `stiffness` interface able to evaluate the 

## Build
### Dependencies
The library and executable alone depend only on standard library, standard compilers (tested `g++` and `clang++`). 
However if optional components are enabled, they require further dependencies.
In particular, we use *GoogleTest* for unit testing (`libgtest-dev` on Debian based OS), 
*Doxygen* for generating documentation (packaged `doxygen`, `doxygen-latex`, `graphviz`, `dia` on Debian) and `valgrind` for memory leak checks. 
Furthermore if [code coverage](https://jpesek.gitlabpages.inria.fr/mechanical-networks/coverage/coverage.html) is required, also `gcovr` is required. 

To simplify the dependency dealing for users we also provide a dedicated [docker image](registry.gitlab.inria.fr/jpesek/mechanical-networks/ubuntu/devel) with all dependencies prepared (see also [registries](https://gitlab.inria.fr/jpesek/mechanical-networks/container_registry) ).

### Basic setup
We recommend to have build folder outside of the source code, e.g.
```
Project --+-- source
          |
          +-- build
```
This prevents pollution of source from build files and simplifies `.gitignore`. 

Basic build then use `cmake`, in case of hierarchy above, it can look as something like:
```shell
cd build
cmake ../source/CMake
make -j
make install
```
Where last command allow us to install the library and 
Note that we refer to `CMake` folder inside source. 
We intentionally separated build system from the code in case it needs to be replaced.

In case unit tests are requested they have to be enabled first, the full set of commands then look like:
```shell
cd build
cmake -D INCLUDE_TEST=ON ../source/CMake
make -j
```
This will compile also tests, which will be located inside folder `build/Testing/MechanicalNetworks`.
Either individual executables can be run or we can invoke from `build` folder `ctest .` to run them all:
```shell
cd build
cmake -D INCLUDE_TEST=ON ../source/CMake
make -j
ctest .
```
Tests themselves are scattered across the source code in `*/Tests/*` folders. 
Note that unit tests cover more tests and functionality then tests for binary found in `source/CLI/Tests` as they have much better access to all aspects. 

Lastly one can also generate documentation by
```shell
cd build
cmake -D INCLUDE_DOCUMENTATION=ON ../source/CMake
make documentation
```
which will generate HTML documentation inside `build/Documentation` folder. 
Documentation can also be [found online](https://jpesek.gitlabpages.inria.fr/mechanical-networks) with [all methods documented](https://jpesek.gitlabpages.inria.fr/mechanical-networks/namespaces.html).

In case you decided to use provided docker image for build, it's recommended to bind the source only as *read-only*.

## Usage
There are 3 supported ways how to use the project:
 * as a binary compiled from source
 * as a library attached to 3rd party project
 * using [binary docker image](registry.gitlab.inria.fr/jpesek/mechanical-networks/mechanical-networks) with the library and application pre-installed 

### Binary 
This is simplest case, binary reads line by line from standard input and for each line computes it's equivalent stiffness which prints to standard output.
If there is an invalid/incomplete or otherwise faulty input. 
Program can be terminated by closing the standard input (e.g. `Ctrl+D` on Linux).
Application terminates with an error message provided by an exception.

### As a library
After the library is successfully built and installed, 
it's header files are exported along with a cmake package. 

Minimal example of `CMakeLists.txt` to successfully link against library is:
```cmake
find_package( MechanicalNetworks REQUIRED )
target_link_library( MyProg PRIVATE MechanicalNetworks::MechanicalNetworks )
```

### Using the docker image
One can run the binary directly from the docker e.g.
```bash
docker run -it registry.gitlab.inria.fr/jpesek/mechanical-networks/mechanical-networks
> echo "[2,3,6]" | stiffness
> echo "(2,3,6)" | stiffness
```
Note, that the binary docker image also have `nginx` server installed hosting the documentation, e.g.:
```bash
docker run -it -p 8080:80 registry.gitlab.inria.fr/jpesek/mechanical-networks/mechanical-networks
```
as long as the docker is running the documentation will be available at [http://localhost:8080](http://localhost:8080).

## Known issues 
Evaluation of `stiffness()` method is limited by the stack. 
At present, at standard Linux configuration, it means maximal depth of the network should not exceed 50000. 
If processing larger network is needed, one can increase the stack size by:
```shell
#Increase to 20 MiB from 10 MiB 
ulimit -s 20480 
```
Note that stack demands scale linearly with depth.
