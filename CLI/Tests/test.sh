#!/bin/bash

#Clean log
if [[ -e output.log ]];
then
    rm output.log 
fi

#Run on known values
echo "==========================================="
echo "Testing on known values:"
cut -d: -f1 values.txt | while read line ;
do 
    echo -n "$line = "
    echo "$line" | stiffness | tee -a output.log
done

if ! ( cut -d: -f2 values.txt | diff output.log - ) ;
then
    echo "Error values are not the same:"
    cut -d: -f2 values.txt | diff output.log -
else
    echo "Values match."
fi

#Generate network
PREFIX="(1,[1,"
SUFFIX="])"
LAST="1"

#Create named pipe
if [[ -f setup.txt ]] ;
then 
    rm setup.txt
fi

#Fill the values
#Value chosen to fit into stack limit
#Stack limit can be increased by ulimit -s ...
#Note that this means 40960 levels of indentation.
for _ in $(seq 1 40960)
do
    echo -n $PREFIX >> setup.txt
done
echo -n $LAST >> setup.txt
for _ in $(seq 1 40960)
do
    echo -n $SUFFIX >> setup.txt
done
echo "==========================================="
echo "Large network: "
cat setup.txt | stiffness 

#Run on error
echo "==========================================="
echo "Testing errors:"
cat errors.txt | while read line ;
do 
    echo -ne "\n\n$line = "
    ( echo "$line" | stiffness ) && echo "Error: Should have failed."
done

