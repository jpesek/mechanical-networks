#include <iomanip>
#include <ios>
#include <iostream>
#include <string>

#include "MechanicalNetworks/stiffness.h"

auto main() -> int
{
    //Read first line
    std::string line{};
    while(std::getline(std::cin, line)) {
        std::cout << std::fixed
                  << std::setprecision(1)
                  << MechanicalNetworks::stiffness(line)
                  << "\n";
    }

    return 0;
}
